import HttpClient from '../utils/HttpClient';
import { objectToParams } from '../utils/ParamBuilder';

import { ICohortContext, ICohort, ICohortMember } from './Cohort';
import { IContext } from '../common/Common';

enum ContextScope {
  self = 'self',
  parents = 'parents',
  all = 'all'
}

export default class CohortService {
  private static instance: CohortService
  
  public static getInstance = (httpClient: HttpClient) => {
    if(!CohortService.instance) {
      CohortService.instance = new CohortService(httpClient);
    }
    return CohortService.instance;
  }

  constructor(private httpClient: HttpClient) {}

  /**
   * Search Cohorts
   * 
   * @param {ICohortContext | IContext} context
   * @param {ContextScope} includes {'What other contexts to fetch the frameworks from. (all, parents, self)'}
   * @param {number} limitFrom {'limitfrom we are fetching the records from'}
   * @param {number} limitNum {'Number of records to fetch'}
   * @param {string} query {'Query string'}
   */
  public search = async (
    context: ICohortContext | IContext,
    includes: ContextScope = ContextScope.parents,
    limitFrom: number = 0,
    limitNum: number = 1,
    query: string = ''
  ) => {
    const params = objectToParams({
      wsfunction: 'core_cohort_search_cohorts',
      includes,
      limitfrom: limitFrom,
      limitnum: limitNum,
      query,
      context
    });

    return await this.httpClient.get(params);
  };

  /**
   * Create Cohorts
   * 
   * @param {Cohort[]} cohort
   */
  public create = async (cohorts: ICohort[]) => {
    const params = objectToParams({
      wsfunction: 'core_cohort_create_cohorts',
      cohorts
    });

    return await this.httpClient.get(params);
  };


  public addMembers = async (members: ICohortMember[]) => {
    const params = objectToParams({
      wsfunction: 'core_cohort_add_cohort_members',
      members
    });

    return await this.httpClient.get(params);
  }

  public getMembers = async (cohortids: number[]) => {
    const params = objectToParams({
      wsfunction: 'core_cohort_get_cohort_members',
      cohortids
    })

    return await this.httpClient.get(params);
  }
}

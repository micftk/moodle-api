export interface ICategory {
  name: string;
  parent: number;
}

import CategoryService from '../../src/category/CategoryService';
import HttpClient from '../../src/utils/HttpClient';

describe('CategoryService', () => {
    const HttpClientMock = HttpClient as jest.Mock<HttpClient>;
    const httpClientMock = new HttpClientMock('', '')
    
    const categoryService = new CategoryService(httpClientMock)


    test('search', async () => {
        const result = await categoryService.search([{ key: "1", value: "2" }])
        expect(result).toEqual({})
    })

});

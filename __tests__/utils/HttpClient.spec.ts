import HttpClient from '../../src/utils/HttpClient';
import axios from "axios";

describe('HttpClient', () => {
  const httpClient = new HttpClient('http://localhost/', 'test');

  test('get', async () => {
   

    const res = await httpClient.get({ test: 1 });
    expect(res).toEqual({});
    expect(axios.get).toHaveBeenCalledWith(
      "http://localhost/",
      {"params": {"moodlewsrestformat": "json", "test": 1, "wstoken": "test"}}
    );
  });

});

import HttpClient from '../utils/HttpClient';
import { ICohortContext, ICohort, ICohortMember } from './Cohort';
import { IContext } from '../common/Common';
declare enum ContextScope {
    self = "self",
    parents = "parents",
    all = "all"
}
export default class CohortService {
    private httpClient;
    private static instance;
    static getInstance: (httpClient: HttpClient) => CohortService;
    constructor(httpClient: HttpClient);
    /**
     * Search Cohorts
     *
     * @param {ICohortContext | IContext} context
     * @param {ContextScope} includes {'What other contexts to fetch the frameworks from. (all, parents, self)'}
     * @param {number} limitFrom {'limitfrom we are fetching the records from'}
     * @param {number} limitNum {'Number of records to fetch'}
     * @param {string} query {'Query string'}
     */
    search: (context: IContext | ICohortContext, includes?: ContextScope, limitFrom?: number, limitNum?: number, query?: string) => Promise<any>;
    /**
     * Create Cohorts
     *
     * @param {Cohort[]} cohort
     */
    create: (cohorts: ICohort[]) => Promise<any>;
    addMembers: (members: ICohortMember[]) => Promise<any>;
    getMembers: (cohortids: number[]) => Promise<any>;
}
export {};

import { ITypeValue } from "../common/Common";
export interface ICohortContext {
    contextlevel: string;
    instanceid: number;
}
export interface ICohort {
    categorytype: ITypeValue;
    name: string;
    idnumber: string;
    visible: boolean;
    description?: string;
    descriptionformat?: string;
    theme?: string;
}
export interface ICohortMember {
    cohorttype: ITypeValue;
    usertype: ITypeValue;
}

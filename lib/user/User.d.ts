export interface IUser {
    id: number;
    username?: string;
    email?: string;
    customfields?: ICustomField[];
}
export interface ICustomField {
    type: string;
    value: string;
    name: string;
    shortname: string;
}
